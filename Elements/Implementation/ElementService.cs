﻿using System.Collections.ObjectModel;
using System.Linq;

namespace Elements.Implementation
{
    public class ElementService : IElementService
    {
        public Collection<Element> SelectElementsUniqueByNumAndAge(Collection<Element> inputElements)
        {
            if (inputElements == null)
            {
                return new Collection<Element>();
            }

            var sutableElements = inputElements
                .Where(x => x.Age > 20)
                .GroupBy(x => x.Num);

            var result = new Collection<Element>();
            foreach (var sutableElement in sutableElements)
            {
                result.Add(sutableElement.Count() == 1
                    ? sutableElement.Single()
                    : sutableElement.First());
            }
            return result;
        }
    }
}