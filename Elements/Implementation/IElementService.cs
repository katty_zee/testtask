﻿using System.Collections.ObjectModel;

namespace Elements.Implementation
{
    public interface IElementService
    {
        Collection<Element> SelectElementsUniqueByNumAndAge(Collection<Element> inputElements);
    }
}