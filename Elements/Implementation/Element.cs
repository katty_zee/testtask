﻿namespace Elements.Implementation
{
    public class Element
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}