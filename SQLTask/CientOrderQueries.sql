﻿create table [Clients](
    [client_id] [uniqueidentifier] not null,
    [client_name] nvarchar(1000) not null,
    constraint [pk_clients] primary key clustered ([client_id] asc)
)
go

create table [Orders](
    [order_id] [uniqueidentifier] not null,
	[client_id] [uniqueidentifier] not null,
	[order_sum] decimal(18, 2) not null,
    [order_date] datetime2 not null,
    constraint [pk_orders] primary key clustered ([order_id] asc)
)
go

-- <Data for test>
insert into Clients (client_id, client_name) values
('7868af63-6b3a-4574-9be4-34cdd2eb240b', 'client1'),
('5a44943b-b218-48f7-820f-795e000ed78b', 'client2'),
('0c82e19e-fb1d-400e-a4a5-f474db8dd8a2', 'client3'),
('3b60c2b7-0713-4394-91ce-3439f29c0dd2', 'client4')

insert into Orders (order_id, client_id, order_sum, order_date) values
(NEWID(), '7868af63-6b3a-4574-9be4-34cdd2eb240b', 10, GETUTCDATE()),
(NEWID(), '7868af63-6b3a-4574-9be4-34cdd2eb240b', 20, GETUTCDATE()),
(NEWID(), '7868af63-6b3a-4574-9be4-34cdd2eb240b', 30, GETUTCDATE()),
(NEWID(), '7868af63-6b3a-4574-9be4-34cdd2eb240b', 25, GETUTCDATE()),
(NEWID(), '7868af63-6b3a-4574-9be4-34cdd2eb240b', 25, GETUTCDATE()),
(NEWID(), '5a44943b-b218-48f7-820f-795e000ed78b', 50, GETUTCDATE()),

(NEWID(), '7868af63-6b3a-4574-9be4-34cdd2eb240b', 50, GETUTCDATE()),

(NEWID(), '0c82e19e-fb1d-400e-a4a5-f474db8dd8a2', 51, GETUTCDATE()),

(NEWID(), '3b60c2b7-0713-4394-91ce-3439f29c0dd2', 60, GETUTCDATE()),
(NEWID(), '3b60c2b7-0713-4394-91ce-3439f29c0dd2', 70, GETUTCDATE()),
(NEWID(), '3b60c2b7-0713-4394-91ce-3439f29c0dd2', 71, GETUTCDATE())
-- </Data for test>

--a. list of clients, which have an order with order_sum > 50 
select distinct c.* 
from 
	Clients c
join 
	Orders o on o.client_id = c.client_id
where 
	o.order_sum > 50


--b. clients, whose total sum of orders exceeds 100
select distinct c.* from
	(select 
		client_id
	from 
		Orders
	group by 
		client_id
	having 
		sum(order_sum) > 100) 
	constantclients
join
	Clients c on c.client_id = constantclients.client_id
