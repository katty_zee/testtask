﻿using BinaryTrees.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinaryTreesTest.Tests
{
    [TestClass]
    public class BinaryTreesComparerTest
    {
        [TestMethod]
        public void TestCompareWhenAllNull()
        {
            var binaryTreesComparer = new BinaryTreesComparer();
            var result = binaryTreesComparer.Compare(null, null);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestCompareWhenFirstNull()
        {
            var binaryTreesComparer = new BinaryTreesComparer();
            var result = binaryTreesComparer.Compare(null, new BTN());
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestCompareWhenSecondNull()
        {
            var binaryTreesComparer = new BinaryTreesComparer();
            var result = binaryTreesComparer.Compare(new BTN(), null);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestCompareWhenNotEqual()
        {
            var binaryTreesComparer = new BinaryTreesComparer();
            var first = Create(300, right: Create(20, Create(11, Create(1)), Create(12, right: Create(3))));
            var second = Create(300, right: Create(20, Create(11, Create(1)), Create(12, Create(2))));
            var result = binaryTreesComparer.Compare(first, second);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestCompareWhenEqual()
        {
            var binaryTreesComparer = new BinaryTreesComparer();
            var first = Create(300, right: Create(20, Create(11, Create(1)), Create(12, right: Create(2))));
            var second = Create(300, right: Create(20, Create(11, Create(1)), Create(12, right: Create(2))));
            var result = binaryTreesComparer.Compare(first, second);
            Assert.IsTrue(result);
        }

        private static BTN Create(int? val = null, BTN left = null, BTN right = null) => new BTN { Val = val ?? 0, Left = left, Right = right};
    }
}
