﻿using System.Collections.ObjectModel;
using Elements.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ElementsTests.Tests
{
    [TestClass]
    public class ElementServiceTest
    {
        [TestMethod]
        public void TestSelectElementsUniqueByNumAndAgeWhereNull()
        {
            var elementService = new ElementService();
            var result = elementService.SelectElementsUniqueByNumAndAge(null);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void TestSelectElementsUniqueByNumAndAgeWhereEmpty()
        {
            var elementService = new ElementService();

            var inputElements = new Collection<Element>();
            var result = elementService.SelectElementsUniqueByNumAndAge(inputElements);
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public void TestSelectElementsUniqueByNumAndAgeWhereFilterWork()
        {
            var elementService = new ElementService();

            var elementFirstIn = Create(21, 10, "name2");
            var elementFirstNotIn = Create();
            var elementSecondNotIn = Create();
            var elementThirdNotIn = Create(20, 10, "name1");
            var inputElements = new Collection<Element>
            {
                elementFirstNotIn,
                elementSecondNotIn,
                elementThirdNotIn,
                elementFirstIn,
                Create(25, 11, "name3"),
                Create(40, 11, "name4"),
            };
            var result = elementService.SelectElementsUniqueByNumAndAge(inputElements);
            CollectionAssert.AllItemsAreNotNull(result);
            CollectionAssert.AllItemsAreUnique(result);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(Element));
            CollectionAssert.Contains(result, elementFirstIn);
            CollectionAssert.DoesNotContain(result, elementFirstNotIn);
            CollectionAssert.DoesNotContain(result, elementSecondNotIn);
            CollectionAssert.DoesNotContain(result, elementThirdNotIn);
            Assert.AreEqual(result.Count, 2);
        }

        private static Element Create(int? age = null, int? num = null, string name = null) => new Element { Age = age ?? 0, Name = name ?? "name", Num = num ?? 0};
    }
}
