﻿namespace BinaryTrees.Implementation
{
    public class BTN 
    {
        public int Val { get; set; }
        public BTN Left { get; set; }
        public BTN Right { get; set; }
    }
}