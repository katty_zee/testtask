﻿namespace BinaryTrees.Implementation
{
    public interface IBinaryTreesComparer
    {
        bool Compare(BTN first, BTN second);
    }
}