﻿namespace BinaryTrees.Implementation
{
    public class BinaryTreesComparer : IBinaryTreesComparer
    {
        public bool Compare(BTN first, BTN second)
        {
            if (first == null && second == null)
            {
                return true;
            }

            if (first == null || second == null)
            {
                return false;
            }

            if (first.Val != second.Val)
            {
                return false;
            }

            return Compare(first.Left, second.Left) &&
                   Compare(first.Right, second.Right);
        }
    }
}